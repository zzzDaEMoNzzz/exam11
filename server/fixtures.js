const mongoose = require('mongoose');
const config = require('./config');

const Product = require('./models/Product');
const Category = require('./models/Category');
const User = require('./models/User');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const [user1, user2] = await User.create(
    {
      username: 'John',
      password: '123',
      display_name: 'John Doe',
      phone: '+996 555123123',
      token: '12345'
    },
    {
      username: 'Jack',
      password: '123',
      display_name: 'Jack Nicholson',
      phone: '+996 550123456',
      token: '54321'
    }
  );

  const [category1, category2, category3] = await Category.create(
    { name: 'Computers' },
    { name: 'Cars'  },
    { name: 'Other' }
  );

  await Product.create(
    {
      user: user1._id,
      category: category1._id,
      name: 'Computer1',
      description: 'Description1',
      image: '_comp1.jpeg',
      price: 30000,
      date: new Date().toISOString()
    },
    {
      user: user2._id,
      category: category1._id,
      name: 'Computer2',
      description: 'Description2',
      image: '_comp2.jpeg',
      price: 28000,
      date: new Date().toISOString()
    },

    {
      user: user1._id,
      category: category2._id,
      name: 'Car1',
      description: 'Desc1',
      image: '_car1.jpeg',
      price: 350000,
      date: new Date().toISOString()
    },
    {
      user: user2._id,
      category: category2._id,
      name: 'Car2',
      description: 'Desc2',
      image: '_car2.jpeg',
      price: 258000,
      date: new Date().toISOString()
    },

    {
      user: user1._id,
      category: category3._id,
      name: 'Something1',
      description: 'Test',
      image: '_no_image.jpeg',
      price: 1000,
      date: new Date().toISOString()
    },
    {
      user: user2._id,
      category: category3._id,
      name: 'Something2',
      description: 'Test',
      image: '_no_image.jpeg',
      price: 1500,
      date: new Date().toISOString()
    },
  );

  return connection.close();
};

run().catch(error => {
  console.error('Something wrong happened...', error);
});