const express = require('express');
const upload = require('../upload');
const auth = require('../middleware/auth');
const Product = require('../models/Product');

const router = express.Router();

router.get('/', (req, res) => {
  const category = req.query.category;

  Product.find(category ? {category} : null, null, {sort: {date: 'DESC'}})
    .then(products => res.send(products))
    .catch(() => res.sendStatus(500));
});

router.get('/:id', (req, res) => {
  Product.findById(req.params.id).populate(['category', 'user']).lean()
    .then(product => {
      if (product) {
        delete product.user.token;
        delete product.user.password;

        res.send(product);
      } else res.sendStatus(404);
    })
    .catch(() => res.sendStatus(500));
});

router.post('/', auth, upload.single('image'), (req, res) => {
  const productData = req.body;

  if (req.file) {
    productData.image = req.file.filename;
  }

  productData.user = req.user._id;
  productData.date = new Date().toISOString();

  const product = new Product(productData);

  product.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

router.delete('/:id', auth, (req, res) => {
  Product.findById(req.params.id)
    .then(product => {
      if (product) {
        if (!req.user._id.equals(product.user)) {
          return res.sendStatus(403);
        }

        Product.deleteOne({_id: product._id})
          .then(result => res.send(result))
          .catch(() => res.sendStatus(500));
      } else res.sendStatus(404);
    })
    .catch(() => res.sendStatus(500));
});


module.exports = router;
