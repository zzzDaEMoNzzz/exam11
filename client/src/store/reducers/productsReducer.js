import {
  CLEAN_PRODUCT_INFO,
  FETCH_CATEGORIES_SUCCESS, FETCH_PRODUCT_INFO_SUCCESS,
  FETCH_PRODUCTS_SUCCESS
} from "../actions/productsActions";

const initialState = {
  products: [],
  categories: [],
  productInfo: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCTS_SUCCESS:
      return {...state, products: action.products};
    case FETCH_CATEGORIES_SUCCESS:
      return {...state, categories: action.categories};
    case FETCH_PRODUCT_INFO_SUCCESS:
      return {...state, productInfo: action.productInfo};
    case CLEAN_PRODUCT_INFO:
      return {...state, productInfo: null};
    default:
      return state;
  }
};

export default reducer;