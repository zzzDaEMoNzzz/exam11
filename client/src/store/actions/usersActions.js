import axios from 'axios';
import {push} from 'connected-react-router';
import {NotificationManager} from 'react-notifications';

export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

const registerUserSuccess = user => ({type: REGISTER_USER_SUCCESS, user});
const registerUserFailure = error => ({type: REGISTER_USER_FAILURE, error});

export const registerUser = userData => {
  return dispatch => {
    return axios.post('/users', userData).then(
      response => {
        dispatch(registerUserSuccess(response.data));
        NotificationManager.success('Registered successfully!');
        dispatch(push('/'));
      },
      error => {
        if (error.response) {
          dispatch(registerUserFailure(error.response.data))
        } else {
          dispatch(registerUserFailure({global: 'No connection'}))
        }
      }
    )
  }
};

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';

const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});

export const loginUser = userData => {
  return dispatch => {
    return axios.post('/users/sessions', userData).then(
      response => {
        dispatch(loginUserSuccess(response.data));
        NotificationManager.success('Logged in successfully!');
        dispatch(push('/'));
      },
      error => {
        if (error.response) {
          NotificationManager.error(error.response.data.error, 'Authentication error!', 5000);
        } else {
          NotificationManager.error('No connection', 'Authentication error!', 5000);
        }
      }
    );
  }
};

export const LOGOUT_USER = 'LOGOUT_USER';

export const logoutUser = () => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
    const config = {headers: {Authorization: token}};

    return axios.delete('/users/sessions', config).then(
      () => {
        dispatch({type: LOGOUT_USER});
        NotificationManager.success('Logged out!');
      },
      () => {
        NotificationManager.error('Could not logout');
      }
    );
  };
};

export const checkAuth = () => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;

    axios.get('/users/sessions', {headers: {Authorization: token}}).then(
      null,
      () => dispatch({type: LOGOUT_USER})
    );
  };
};