import axios from 'axios';
import {push} from 'connected-react-router';
import {NotificationManager} from 'react-notifications';

export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';

const fetchProductsSuccess = products => ({type: FETCH_PRODUCTS_SUCCESS, products});

export const fetchProducts = categoryId => {
  return dispatch => {
    let url = '/products/';
    if (categoryId) url += `?category=${categoryId}`;

    axios.get(url).then(
      response => dispatch(fetchProductsSuccess(response.data))
    );
  };
};

export const FETCH_CATEGORIES_SUCCESS = 'FETCH_CATEGORIES_SUCCESS';

const fetchCategoriesSuccess = categories => ({type: FETCH_CATEGORIES_SUCCESS, categories});

export const fetchCategories = () => {
  return dispatch => {
    axios.get('/categories').then(
      response => dispatch(fetchCategoriesSuccess(response.data))
    );
  };
};

export const addProduct = productData => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;

    axios.post('/products', productData, {headers: {Authorization: token}}).then(
      () => {
        dispatch(push('/products'));
        NotificationManager.success('Product added!');
      },
      error => {
        if (error.response) {
          const errorData = error.response.data;
          const errorMessage = errorData.error || errorData.errors[Object.keys(errorData.errors)[0]].message;

          if (errorMessage) NotificationManager.error(errorMessage);
        } else {
          NotificationManager.error('No connection');
        }
      }
    );
  };
};

export const FETCH_PRODUCT_INFO_SUCCESS = 'FETCH_PRODUCT_INFO_SUCCESS';

const fetchProductInfoSuccess = productInfo => ({type: FETCH_PRODUCT_INFO_SUCCESS, productInfo});

export const fetchProductInfo = productId => {
  return dispatch => {
    axios.get(`/products/${productId}`).then(
      response => dispatch(fetchProductInfoSuccess(response.data))
    );
  };
};

export const CLEAN_PRODUCT_INFO = 'CLEAN_PRODUCT_INFO';

export const cleanProductInfo = () => ({type: CLEAN_PRODUCT_INFO});

export const deleteProduct = id => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;

    axios.delete(`/products/${id}`, {headers: {Authorization: token}}).then(
      () => {
        dispatch(push('/products'));
        NotificationManager.success('Product deleted!');
      },
      error => {
        if (error.response) {
          const errorData = error.response.data;
          const errorMessage = errorData.error || errorData.errors[Object.keys(errorData.errors)[0]].message;

          if (errorMessage) NotificationManager.error(errorMessage);
        } else {
          NotificationManager.error('No connection');
        }
      }
    );
  };
};