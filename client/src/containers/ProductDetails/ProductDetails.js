import React, {Component} from 'react';
import moment from 'moment';
import './ProductDetails.css';
import {connect} from "react-redux";
import {cleanProductInfo, deleteProduct, fetchProductInfo} from "../../store/actions/productsActions";
import {apiURL} from "../../constants";

class ProductDetails extends Component {
  componentDidMount() {
    const productId = this.props.match.params.id;
    this.props.getProductInfo(productId);
  }

  componentWillUnmount() {
    this.props.cleanProductInfo();
  }

  deleteProduct = id => {
    if (window.confirm('Are you sure you want to delete the product?')) {
      this.props.deleteProduct(id);
    }
  };

  render() {
    if (!this.props.productInfo) return <h2>Loading...</h2>;

    return (
      <div className="ProductDetails">
        <div className="ProductDetails-header">
          <span>Published:</span>
          <span>
            {moment(this.props.productInfo.date).format('DD.MM.YYYY HH:mm')}
          </span>
          <span>by <strong>{this.props.productInfo.user.display_name}</strong></span>
          <span>in {this.props.productInfo.category.name}</span>
        </div>
        <img
          src={`${apiURL}/uploads/${this.props.productInfo.image}`}
          alt=""
          className="ProductDetails-image"
        />
        <h2>{this.props.productInfo.name}</h2>
        <p>{this.props.productInfo.description}</p>
        <p>Price: <span>{this.props.productInfo.price} KGS</span></p>
        <p>Telephone: <span>{this.props.productInfo.user.phone}</span></p>
        <div className="ProductDetails-footer">
          {this.props.user && (this.props.user._id === this.props.productInfo.user._id) && (
            <button
              onClick={() => this.deleteProduct(this.props.productInfo._id)}
            >
              Delete product
            </button>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  productInfo: state.products.productInfo,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  getProductInfo: productId => dispatch(fetchProductInfo(productId)),
  cleanProductInfo: () => dispatch(cleanProductInfo()),
  deleteProduct: id => dispatch(deleteProduct(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails);