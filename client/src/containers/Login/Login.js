import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import './Login.css';

import FormElement from "../../components/FormElement/FormElement";
import {loginUser} from "../../store/actions/usersActions";

class Login extends Component {
  state = {
    username: '',
    password: ''
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  submitFormHandler = event => {
    event.preventDefault();

    this.props.loginUser({...this.state});
  };

  render() {
    return (
      <Fragment>
        <h2>Login</h2>
        <form className="Login" onSubmit={this.submitFormHandler}>
          <FormElement
            title="Username"
            propertyName="username"
            type="text"
            value={this.state.username}
            onChange={this.inputChangeHandler}
            required
          />
          <FormElement
            title="Password"
            propertyName="password"
            type="password"
            value={this.state.password}
            onChange={this.inputChangeHandler}
            required
          />
          <button>Login</button>
        </form>
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  loginUser: userData => dispatch(loginUser(userData))
});

export default connect(null, mapDispatchToProps)(Login);