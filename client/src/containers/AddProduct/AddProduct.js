import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import './AddProduct.css';
import {addProduct, fetchCategories} from "../../store/actions/productsActions";

class AddProduct extends Component {
  state = {
    name: '',
    description: '',
    price: '',
    category: '',
    image: null,
    fileName: null
  };

  componentDidMount() {
    this.props.getCategories();
  }

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.addProduct(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      image: event.target.files[0],
      fileName: event.target.files[0] && event.target.files[0].name
    })
  };

  render() {
    if (!this.props.user) {
      this.props.history.replace('/');
    }

    if (this.props.categories.length === 0) return <h2>Loading...</h2>;

    return (
      <Fragment>
        <h2>Create new item</h2>
        <form
          className="AddProduct"
          onSubmit={this.submitFormHandler}
        >
          <label htmlFor="name">Title</label>
          <input
            type="text"
            id="name"
            name="name"
            value={this.state.name}
            onChange={this.inputChangeHandler}
            required
          />
          <label htmlFor="description">Description</label>
          <textarea
            name="description"
            id="description"
            value={this.state.description}
            onChange={this.inputChangeHandler}
            rows="5"
            required
          />
          <label htmlFor="price">Price</label>
          <input
            type="number"
            id="price"
            name="price"
            value={this.state.number}
            onChange={this.inputChangeHandler}
            required
          />
          <label htmlFor="image" className="AddProduct-uploadFile">
            <span className="AddProduct-uploadLabelName">Image</span>
            <div className="AddProduct-uploadBtn">Upload file</div>
            <span className="AddProduct-fileName">{this.state.fileName}</span>
            <input
              type="file"
              id="image"
              name="image"
              onChange={this.fileChangeHandler}
              required
            />
          </label>
          <div className="AddProduct-category">
            <label>Category</label>
            <select
              name="category"
              value={this.state.category}
              onChange={this.inputChangeHandler}
              required
            >
              <option value="" hidden>Select category</option>
              {this.props.categories.map(category => (
                <option key={category._id} value={category._id}>{category.name}</option>
              ))}
            </select>
            <button className="AddProduct-postBtn">Create post</button>
          </div>
        </form>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  categories: state.products.categories,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  getCategories: () => dispatch(fetchCategories()),
  addProduct: productData => dispatch(addProduct(productData))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddProduct);