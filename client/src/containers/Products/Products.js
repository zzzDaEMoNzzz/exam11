import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchCategories, fetchProducts} from "../../store/actions/productsActions";
import './Products.css';
import Product from "./Product/Product";
import Categories from "../../components/Categories/Categories";

class Products extends Component {
  componentDidMount() {
    const category = this.props.match.params.category;
    this.props.getProducts(category);

    this.props.getCategories();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const category = this.props.match.params.category;
    if (prevProps.match.params.category !== category) {
      this.props.getProducts(category);
    }
  }

  render() {
    const products = this.props.products.map(productData => (
      <div key={productData._id}>
        <Product data={productData}/>
      </div>
    ));

    return (
      <div className="Products">
        <Categories data={this.props.categories}/>
        <div>
          {products}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.products.products,
  categories: state.products.categories
});

const mapDispatchToProps = dispatch => ({
  getProducts: categoryId => dispatch(fetchProducts(categoryId)),
  getCategories: () => dispatch(fetchCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(Products);