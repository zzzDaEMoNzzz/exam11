import React from 'react';
import {Link} from "react-router-dom";
import './Product.css';
import {apiURL} from "../../../constants";

const Product = ({data}) => {
  const imageUrl = `${apiURL}/uploads/${data.image}`;

  return (
    <Link to={`/product/${data._id}`} className="Product">
      <img src={imageUrl} style={data.image ? null : {border: 'none'}} alt=""/>
      <div>
        <h4>{data.name}</h4>
        <p className="Product-price">{data.price} KGS</p>
      </div>
    </Link>
  );
};

export default Product;
