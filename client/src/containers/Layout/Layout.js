import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import './Layout.css';
import Header from "../../components/Header/Header";
import {logoutUser} from "../../store/actions/usersActions";

class Layout extends Component {
  render() {
    return (
      <Fragment>
        <Header user={this.props.user} logout={this.props.logoutUser}/>
        <div className="Layout-body">
          {this.props.children}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(logoutUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(Layout);