import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import './Register.css';
import {registerUser} from "../../store/actions/usersActions";
import FormElement from "../../components/FormElement/FormElement";

class Register extends Component {
  state = {
    username: '',
    password: '',
    display_name: '',
    phone: ''
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  submitFormHandler = event => {
    event.preventDefault();

    this.props.registerUser({...this.state});
  };

  getFieldError = fieldName => {
    return this.props.error && this.props.error.errors && this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message;
  };

  render() {
    return (
      <Fragment>
        <h2>Register new user</h2>
        <form className="Register" onSubmit={this.submitFormHandler}>
          <FormElement
            title="Username"
            propertyName="username"
            type="text"
            value={this.state.username}
            onChange={this.inputChangeHandler}
            error={this.getFieldError('username')}
            required
          />
          <FormElement
            title="Password"
            propertyName="password"
            type="password"
            value={this.state.password}
            onChange={this.inputChangeHandler}
            error={this.getFieldError('password')}
            required
          />
          <FormElement
            title="Display name"
            propertyName="display_name"
            type="text"
            value={this.state.display_name}
            onChange={this.inputChangeHandler}
            error={this.getFieldError('display_name')}
            required
          />
          <FormElement
            title="Phone number"
            propertyName="phone"
            type="text"
            value={this.state.phone}
            onChange={this.inputChangeHandler}
            error={this.getFieldError('phone')}
            required
          />
          <button>Register</button>
        </form>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
  registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);