import React, {Component} from 'react';
import {connect} from "react-redux";
import {Switch, Route} from 'react-router-dom';
import {NotificationContainer} from "react-notifications";
import {checkAuth} from "./store/actions/usersActions";

import './App.css';
import 'react-notifications/lib/notifications.css';

import Layout from "./containers/Layout/Layout";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Products from "./containers/Products/Products";
import AddProduct from "./containers/AddProduct/AddProduct";
import ProductDetails from "./containers/ProductDetails/ProductDetails";

class App extends Component {
  componentDidMount() {
    if (this.props.user) {
      this.props.checkAuth();
    }
  }

  render() {
    return (
      <div className="App">
        <NotificationContainer/>
        <Layout>
          <Switch>
            <Route path="/" exact component={Products}/>
            <Route path="/products/add" exact component={AddProduct}/>
            <Route path="/products/:category" exact component={Products}/>
            <Route path="/products" exact component={Products}/>
            <Route path="/product/:id" exact component={ProductDetails}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <Route render={() => <div>Page not found!</div>}/>
          </Switch>
        </Layout>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  checkAuth: () => dispatch(checkAuth())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);