import React from 'react';
import ReactDOM from 'react-dom';
import {apiURL} from "./constants";
import {ConnectedRouter} from "connected-react-router";
import {Provider} from 'react-redux';

import store, {history} from "./store/configureStore";
import App from './App';

import axios from 'axios';
axios.defaults.baseURL = apiURL;

const app = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App/>
    </ConnectedRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));