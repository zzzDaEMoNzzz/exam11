import React from 'react';
import {NavLink, withRouter} from "react-router-dom";
import './Categories.css';

const Categories = props => {
  const isAllCategory = () => {
    return props.history.location.pathname === '/products/' || props.history.location.pathname === '/';
  };

  return (
    <div className="Categories">
      <ul>
        <li>
          <NavLink
            to="/products/"
            isActive={isAllCategory}
          >
            All
          </NavLink>
        </li>
        {props.data.map(category => (
          <li key={category._id}>
            <NavLink to={`/products/${category._id}`}>
              {category.name}
            </NavLink>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default withRouter(Categories);